$(document).ready(function () {
  var win = $(window);

  // Each time the user scrolls
  win.scroll(function () {
    // End of the document reached?
    if ($(document).height() - win.height() == win.scrollTop()) {
      $('#loading').show();
      //alert($("div[id='new']").length);

      //alert($("div[class='col-sm-6']").length);
      const offset = $("div[id='scroll']").length;
      const limit = $("div[id='scroll']").length + 7;

      $.ajax({
        url: 'https://api.covid19api.com/summary',
        dataType: 'json',
        success: function (json) {
          const NewNata = json.Countries.slice(offset, limit);
          const continar = NewNata.map((e, i) => {
            if (e.Country === 'Israel') return true;
            const CollapseId = Math.floor(Math.random() * 10000);
            let continar =
              '<div id="scroll" class="col-sm-4"><div class="card text-center">';
            continar += '<div class="card-body text-center">';
            continar += `<img src="./flags/${e.CountryCode.toLowerCase()}.svg" class="card-img-top" alt="..." />`;
            continar += `<h5 class="card-title">${e.Country}</h5>`;
            continar += `<div id="Collapse${CollapseId}" class="collapse text-center">`;
            continar += '<div class="card">';
            continar += '<ul class="list-group list-group-flush">';
            continar += `<li class="list-group-item">NewConfirmed : ${SepComma(
              e.NewConfirmed
            )}</li>`;
            continar += `<li class="list-group-item">TotalConfirmed : ${SepComma(
              e.TotalConfirmed
            )}</li>`;
            continar += `<li class="list-group-item">NewDeaths : ${SepComma(
              e.NewDeaths
            )}</li>`;
            continar += `<li class="list-group-item">TotalDeaths : ${SepComma(
              e.TotalDeaths
            )}</li>`;
            continar += `<li class="list-group-item">NewRecovered : ${SepComma(
              e.NewRecovered
            )}</li>`;
            continar += `<li class="list-group-item">TotalRecovered : ${SepComma(
              e.TotalRecovered
            )}</li>`;
            continar += `</ul></div></div>`;
            continar += ` <button type="button" style="margin-top: 2%;" class="btn btn-info" data-toggle="collapse" aria-expanded="false" data-target="#Collapse${CollapseId}"> Info </button></div></div></div>`;
            $('#posts').append(continar);
          });
          $('#loading').hide();
        }
      });
    }
  });
});

function SepComma(str) {
  let makeStr = str.toString();
  let strArray = makeStr.split(/(?=(?:...)*$)/);
  let joinStr = strArray.join(',');
  return joinStr;
}
