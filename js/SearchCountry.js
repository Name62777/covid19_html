$(function () {
  $('#SearchCountry').click(function () {
    $('#SearchLoading').show();
    const CountryName = $('#Country').val();
    var replaced = CountryName.replace(' ', '-');
    $.ajax({
      url: `https://api.covid19api.com/total/country/${replaced}`,
      dataType: 'json',
      success: function (json) {
        $('#DeathsByCountry').text(
          `Deaths: ${SepComma(json[json.length - 1].Deaths)}`
        );
        $('#ConfirmedByCountry').text(
          `Confirmed: ${SepComma(json[json.length - 1].Confirmed)}`
        );

        $('#RecoveredByCountry').text(
          `Recovered: ${SepComma(json[json.length - 1].Recovered)}`
        );
        //Country
        $('#ActiveByCountry').text(
          `Active: ${SepComma(json[json.length - 1].Active)}`
        );

        $('#CountryName').text(`${json[json.length - 1].Country}`);
        $('#SearchLoading').hide();
      },
      error: function (data) {
        $('#searchLoadError').text('There is problem to loading page');
        $('#SearchLoading').hide();
      }
    });
  });
});

function SepComma(str) {
  let makeStr = str.toString();
  let strArray = makeStr.split(/(?=(?:...)*$)/);
  let joinStr = strArray.join(',');
  return joinStr;
}
