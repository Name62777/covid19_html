$(function () {
  $.ajax({
    url: 'https://api.covid19api.com/summary',
    dataType: 'json',
    success: function (json) {
      $('#pageLoadError').hide();
      $('#NewConfirmed').text(
        `NewConfirmed: ${SepComma(json.Global.NewConfirmed)}`
      );
      $('#TotalConfirmed').text(
        `TotalConfirmed: ${SepComma(json.Global.TotalConfirmed)}`
      );
      $('#NewDeaths').text(`NewDeaths: ${SepComma(json.Global.NewDeaths)}`);
      $('#TotalDeaths').text(
        `TotalDeaths: ${SepComma(json.Global.TotalDeaths)}`
      );
      $('#NewRecovered').text(
        `NewRecovered: ${SepComma(json.Global.NewRecovered)}`
      );
      $('#TotalRecovered').text(
        `TotalRecovered: ${SepComma(json.Global.TotalRecovered)}`
      );
      const finalDate = getToday(json.Date);
      $('#todayDate').text(finalDate);

      $('#TodayLoading').hide();
    },
    error: function (data) {
      $('#pageLoadError').text('There is problem to loading page');
    }
  });

  $.ajax({
    url: `https://api.covid19api.com/total/country/iran`,
    dataType: 'json',
    success: function (json) {
      $('#DeathsByCountry').text(
        `Deaths: ${SepComma(json[json.length - 1].Deaths)}`
      );
      $('#ConfirmedByCountry').text(
        `Confirmed: ${SepComma(json[json.length - 1].Confirmed)}`
      );

      $('#RecoveredByCountry').text(
        `Recovered: ${SepComma(json[json.length - 1].Recovered)}`
      );
      //Country
      $('#ActiveByCountry').text(
        `Active: ${SepComma(json[json.length - 1].Active)}`
      );

      $('#CountryName').text(`${json[json.length - 1].Country}`);

      $('#SearchLoading').hide();
    },
    error: function (data) {
      $('#searchLoadError').text('There is problem to loading page');
      $('#SearchLoading').hide();
    }
  });
});

function SepComma(str) {
  let makeStr = str.toString();
  let strArray = makeStr.split(/(?=(?:...)*$)/);
  let joinStr = strArray.join(',');
  return joinStr;
}

function getToday(date) {
  const todayDate = new Date(date);
  const day =
    todayDate.getDate().toString().length !== 1
      ? todayDate.getDate()
      : '0' + todayDate.getDate();

  const month =
    todayDate.getMonth().toString().length !== 1
      ? todayDate.getMonth() + 1
      : '0' + (todayDate.getMonth() + 1);

  const year = todayDate.getFullYear();

  const returnData = `${year}/${month}/${day}`;

  return returnData;
}
