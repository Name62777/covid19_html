# covid360

- Main file of project is index.html

./

```bash
├───css
├───flags
├───font
├───js
├───pic

```

- css:All project css file is there

  - AutoComplete.css
  - main.css

- flags:Countries flags is there.

  - ...

- font:font of project is there.

  - Acme.ttf

- js:All javascript files is there.

  - AutoComplete.js : Search by country autoComplete handle bythis file
  - InfiniteScroll.js : Infinte scroll handle by this file
  - SearchCountry.js : Search country handle by thsi file
  - TopInfo.js : Onload data handel by this file

- pic:all picture of project is there.
